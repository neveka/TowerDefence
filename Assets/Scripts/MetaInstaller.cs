using UnityEngine;
using Zenject;

public class ObjectCreator
{
	private readonly DiContainer _container;
	public ObjectCreator(DiContainer container)
	{
		_container = container;
	}

	T CreateTransient<T>()
	{
		_container.Bind<T> ().AsTransient ();
		return _container.Resolve<T> ();
	}
}

public class MetaInstaller : MonoInstaller<MetaInstaller>
{
    public override void InstallBindings()
    {
		Container.Bind<ObjectCreator> ().AsSingle ();
		 
		Container.Bind<IHeartBeat> ().To<HeartBeat> ().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<Player>().To<Player> ().AsSingle();

		Container.Bind<TanksConfig>().FromScriptableObjectResource("TanksConfig").AsSingle();
		Container.Bind<IPath>().To<Path>().AsSingle();
		Container.Bind<ITanksManager>().To<TanksManager>().AsSingle().NonLazy();;
		Container.Bind<TowersManager> ().AsSingle ().NonLazy();;

		Container.Bind<ITuneLevelController>().To<TuneLevelController>().AsSingle().NonLazy();;
		Container.Bind<HUDController>().To<HUDController>().AsSingle().NonLazy();;
    }
}