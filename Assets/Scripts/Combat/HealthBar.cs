﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour 
{
	public SpriteRenderer sprite;
	public void SetValue(float value)
	{
		sprite.transform.localScale = new Vector3(value*2, 0.5f, 1);
	}
}
