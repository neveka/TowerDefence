﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player 
{
	public event System.Action<int> MoneyChanged;
	public event System.Action<int> HealthChanged;

	private int _health = 100;
	private int _money = 100;

	public int Money
	{
		set { 
			if (_money == value)
				return;
			_money = value; 
			if (MoneyChanged != null)
				MoneyChanged (_money);}
		get{ return _money; }
	}

	public int Health
	{
		set { 
			if (value < 0)
				value = 0;
			if (_health == value)
				return;
			_health = value; 
			if (HealthChanged != null)
				HealthChanged (_health);}
		get{ return _health; }
	}

	public void OnDamage(int damage)
	{
		Health -= damage;
	}
}
