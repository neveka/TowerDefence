﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathVisual : MonoBehaviour
{
	public GameObject pathTilePrefab;
	public float tilesInMeter = 8;

	public List<Vector3> FindPoints() 
	{
		return new List<Vector3>(System.Array.ConvertAll( transform.GetComponentsInChildren<SpriteRenderer> (), (SpriteRenderer s)=>s.transform.position ));
	}

	public float GetAngle(Vector3 fromVertex, Vector3 toVertex)
	{
		float angle = Mathf.Atan ((fromVertex - toVertex).z / (fromVertex - toVertex).x) * 180 / Mathf.PI;
		if (fromVertex.x > toVertex.x)
			angle += 180;
		return angle;
	}

	public void ConnectPoints(Vector3 fromVertex, Vector3 toVertex)
	{
		Vector3 point = Vector3.zero;
		Vector3 from = fromVertex, to = toVertex;
		float step = 1f/tilesInMeter;
		int steps = (int)((toVertex-fromVertex).magnitude/step);
		float angle = GetAngle(fromVertex, toVertex);
		for(int i=0; i<steps; i++)
		{
			float p = ((float)i+1)/steps;
			point = fromVertex*(1-p)+toVertex*p;

			to = i==steps-1?toVertex:point;
			GameObject go = Instantiate(pathTilePrefab);//?
			go.transform.parent = transform;

			go.transform.position = (from + to)/2f;
			go.transform.localScale = new Vector3((from - to).magnitude*tilesInMeter, 1, 1);
			go.transform.Rotate(Vector3.forward, angle);
			from = to;
		}
	}

}
