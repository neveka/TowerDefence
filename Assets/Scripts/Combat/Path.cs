﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPath
{
	event System.Action Created;

	void CreateVisual (string modelName);
	Vector3 GetPointWithDistance (float distance, out float angle);
	float GetAllDistance ();
}

public class Path: IPath
{
	public event System.Action Created;

	private List<Vector3> _points;
	private PathVisual _visual;

	public void CreateVisual(string modelName)
	{
		_visual = GameObject.Instantiate<PathVisual> (Resources.Load<PathVisual> (modelName));
		_points = _visual.FindPoints ();
		for(int i=0; i <_points.Count-1; i++)
		{
			_visual.ConnectPoints(_points[i], _points[i+1]);
		}
		if (Created != null)
			Created ();
	}

	public float GetAllDistance()
	{
		float dist = 0;
		for (int i = 0; i < _points.Count - 1; i++) 
		{
			dist += (_points[i] - _points[i+1]).magnitude;
		}
		return dist;
	}

	public Vector3 GetPointWithDistance (float distance, out float angle)
	{
		angle = 0;
		for (int i = 0; i < _points.Count - 1; i++) 
		{
			float currentDistance = (_points[i] - _points[i+1]).magnitude;
			if (distance < currentDistance) 
			{
				float p = distance / currentDistance;
				angle = _visual.GetAngle (_points [i], _points [i+1]);
				return _points[i]*(1-p)+_points[i+1]*(p);
			} 
			else 
			{
				distance -= currentDistance;
			}
		}
		angle = _visual.GetAngle (_points [_points.Count - 2], _points [_points.Count - 1]);
		return _points [_points.Count - 1];
	}
}
