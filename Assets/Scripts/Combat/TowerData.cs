﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerData
{
	public float shootInterval = 1;
	public float shootSpeed = 1;
	public float radius = 1;
	public int shootDamage = 10;
	public string modelName = "Tower1";
	public int prise = 10;
}
