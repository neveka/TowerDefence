﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankVisual : MonoBehaviour
{
	private Quaternion _startRotation;
	private HealthBar _healthBar;

	void Awake()
	{
		_startRotation = transform.rotation;
		_healthBar = GameObject.Instantiate<HealthBar>(Resources.Load<HealthBar>("HealthBar"));
		_healthBar.transform.parent = transform;
		_healthBar.transform.position = transform.position;
	}

	public void SetHealth(float value)
	{
		_healthBar.SetValue (value);
	}

	public void SetPosition (Vector3 position)
	{
		transform.position = position;
	}

	public void SetRotation (float angle)
	{
		transform.rotation = _startRotation;
		transform.Rotate(Vector3.forward, angle);
	}
}
