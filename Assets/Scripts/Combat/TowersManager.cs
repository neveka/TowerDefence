﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowersManager
{
	private List<Tower> _towers = new List<Tower> ();
	private readonly ITanksManager _tanksManager;
	private readonly Player _player;

	public TowersManager(IHeartBeat heartBeat, ITanksManager tanksManager, Player player)
	{
		heartBeat.UpdateEvent += OnUpdate;
		_tanksManager = tanksManager;
		_player = player;
	}

	void OnUpdate()
	{
		if (Input.GetMouseButtonDown (0) && BaseWindow.openModalWindows == 0) 
		{
			Vector3 position = Input.mousePosition;
			position.z = 0;
			position = Camera.main.ScreenToWorldPoint(position);
			position.y = 0;

			var towerData = new TowerData ();
			if (_player.Money >= towerData.prise)
			{
				_player.Money -= towerData.prise;
				var tower = new Tower ();
				tower.CreateInPosition (towerData, position);
				tower.BulletExplored += _tanksManager.OnBulletExplored;
				_towers.Add (tower);
			}
		}

		var tankPositions = _tanksManager.GetTanksPositions ();
		if(tankPositions.Count>0)
			_towers.ForEach(t => t.SetTargets (tankPositions));
	}


}