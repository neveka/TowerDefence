﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITank
{
	event System.Action<int> DamagePlayer;

	void Create(TankData tankData);
	void GoAlongPath (IPath path, float delay);
	Vector3 GetPosition();
	void OnDamage (int damage);
	int Health { get; }
}

public class Tank: ITank
{
	public event System.Action<int> DamagePlayer;

	private int _health = 5;
	private float _speed = 1f;
	private float _distance = 0;
	private TankData _tankData;
	private TankVisual _visual;

	public int Health
	{
		get { return _health; }
	}

	public void Create(TankData tankData)
	{
		_tankData = tankData;
		_visual = GameObject.Instantiate<TankVisual>(Resources.Load<TankVisual> (tankData.modelName));
		_visual.SetPosition (Vector3.down);
		_health = _tankData.health;
		_visual.SetHealth (1);
	}

	public void OnDamage(int damage)
	{
		_health -= damage;
		if (_health < 0)
			_health = 0;
		_visual.SetHealth ((float)_health/_tankData.health);
	}

	public Vector3 GetPosition()
	{
		return _visual.transform.position;
	}

	public void GoAlongPath(IPath path, float delay)
	{
		_visual.StartCoroutine (GoAlongPathWithSpeed(path, delay));
	}

	IEnumerator GoAlongPathWithSpeed(IPath path, float delay)
	{
		_distance = 0;
		yield return new WaitForSeconds (delay);
		while (_distance < path.GetAllDistance () && _health>0) 
		{
			_distance += Time.deltaTime * _speed;
			float angle;
			_visual.SetPosition (path.GetPointWithDistance(_distance, out angle));
			_visual.SetRotation (angle);
			yield return null;
		}
		//damage player
		if (_health > 0 && DamagePlayer != null)
			DamagePlayer (_tankData.damageToPlayer);
	}
}