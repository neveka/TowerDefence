﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TankData
{
	public string modelName;
	public int health;
	public float armor;
	public int killPrise;
	public int damageToPlayer;
}
