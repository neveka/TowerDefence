﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerVisual : MonoBehaviour 
{
	public event System.Action<Vector3> BulletExplored;
	public GameObject bulletPrefab;

	public void SetPosition (Vector3 position)
	{
		transform.position = position;
	}

	public void Shoot(float speed, Vector3 endPoint) //bullets ara not logic objects, they are visual effects
	{
		var bullet = GameObject.Instantiate<GameObject>(bulletPrefab);
		StartCoroutine(GoToPointWithSpeed(bullet, speed, endPoint));
	}

	IEnumerator GoToPointWithSpeed(GameObject bullet, float speed, Vector3 endPoint)
	{
		float distance = 0;
		float allDistance = (transform.position - endPoint).magnitude;
		while (distance < allDistance) 
		{
			distance += Time.deltaTime * speed;
			if (distance > allDistance) 
			{
				if (BulletExplored != null)
					BulletExplored (endPoint);
				Destroy (bullet);
			}
			else
			{
				float p = distance / allDistance;
				bullet.transform.position = transform.position*(1-p)+endPoint*p;
			}
			yield return null;
		}
	}
}
