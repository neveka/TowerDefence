﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITower
{
	event System.Action<Vector3, float> BulletExplored;
	void CreateVisualInPosition(string modelName, Vector3 position);
	void SetTargets (List<Vector3> positions);
}

public class Tower
{
	public event System.Action<Vector3, int> BulletExplored;

	private TowerVisual _visual;
	private float _lastShootTime = 0;

	private TowerData _towerData;

	public void CreateInPosition(TowerData towerData, Vector3 position)
	{
		_towerData = towerData;
		_visual = GameObject.Instantiate<TowerVisual>(Resources.Load<TowerVisual> (towerData.modelName));
		_visual.SetPosition (position);
		_visual.BulletExplored += OnBulletExplored;
	}

	public void SetTargets(List<Vector3> positions)
	{
		if (_lastShootTime == 0 || Time.time - _lastShootTime > _towerData.shootInterval) 
		{
			float minSqrMagnitude = float.MaxValue;
			int nearestIndex = -1;
			for (int i = 0; i < positions.Count; i++) 
			{
				float sqrMagnitude = (_visual.transform.position - positions[i]).sqrMagnitude;
				if (sqrMagnitude < minSqrMagnitude) 
				{
					minSqrMagnitude = sqrMagnitude;
					nearestIndex = i;
				}
			}
			if (nearestIndex != -1 && minSqrMagnitude < _towerData.radius * _towerData.radius) 
			{
				_visual.Shoot (_towerData.shootSpeed, positions[nearestIndex]);
				_lastShootTime = Time.time;
			}
		}
	}

	void OnBulletExplored(Vector3 position)
	{
		if (BulletExplored != null)
			BulletExplored (position, _towerData.shootDamage);
	}
}