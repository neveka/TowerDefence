﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITanksManager
{
	void AddWave(WaveData waveData);
	List<Vector3> GetTanksPositions();
	void OnBulletExplored (Vector3 position, int damage);
}

public class WaveData
{
	public int amount;
	public List<float> parts = new List<float>();

	public WaveData()
	{
		amount = 5;
		parts.Add (33);
		parts.Add (33);
		parts.Add (33);
	}

	public int GetIndexInConfig(int indexInWave)
	{
		float p = indexInWave*100/amount;
		for (int j = 0; j < parts.Count; j++) 
		{
			if (p - parts[j] >= 0) 
			{
				p -= parts[j];
			} 
			else 
			{
				return j;
			}
		}
		return 0;//?
	}
}

public class TanksManager: ITanksManager 
{
	private readonly IPath _path;
	private readonly TanksConfig _tanksConfig;
	private readonly Player _player;

	private List<ITank> _tanks = new List<ITank>();

	public TanksManager(IPath path, TanksConfig tanksConfig, Player player)
	{
		_path = path;
		_player = player;
		_tanksConfig = tanksConfig;
	}

	public List<Vector3> GetTanksPositions()
	{
		return _tanks.FindAll(t=>t.Health>0).ConvertAll (t => t.GetPosition ());
	}

	public void AddWave(WaveData waveData)
	{
		for (int i = 0; i < waveData.amount; i++) 
		{
			ITank tank = new Tank ();//todo use pool
			int index = waveData.GetIndexInConfig(i);
			tank.Create(_tanksConfig.tankDatas[index]);
			index = Random.Range (0, _tanks.Count);
			_tanks.Insert (index, tank);
			tank.DamagePlayer += _player.OnDamage;
		}
		for(int i = 0; i < waveData.amount; i++)
			_tanks[i].GoAlongPath (_path, 0.5f*i);
	}

	public void OnBulletExplored(Vector3 position, int damage)
	{
		float radius = 0.3f;
		_tanks.ForEach(t => {if((t.GetPosition() - position).sqrMagnitude<radius*radius)t.OnDamage(damage);});
	}
}
