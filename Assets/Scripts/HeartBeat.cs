﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHeartBeat
{
	event System.Action UpdateEvent; 
}

public class HeartBeat: MonoBehaviour, IHeartBeat
{
	public event System.Action UpdateEvent;  
	void Update()
	{
		if (UpdateEvent != null)
			UpdateEvent ();
	}
}