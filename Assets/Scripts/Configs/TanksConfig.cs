﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TanksConfig", menuName = "ScriptableObject/TanksConfig")]
public class TanksConfig : ScriptableObject 
{
	public List<TankData> tankDatas;
}
