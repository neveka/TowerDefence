﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WavePlate : MonoBehaviour 
{
	public InputField amountInput;
	public List<InputField> percentInputs;
	// Use this for initialization
	void Start () 
	{
		amountInput.text = "10";
		percentInputs.ForEach (i => i.text = "33");
	}
}
