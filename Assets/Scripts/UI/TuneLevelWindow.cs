﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseWindow : MonoBehaviour 
{
	public static int openModalWindows = 0; //todo move to WindowsManager

	protected bool isModal;
	protected virtual void Start()
	{
		RectTransform rectTransform = GetComponent<RectTransform> ();
		rectTransform.SetParent(GameObject.FindObjectOfType<Canvas> ().transform);
		rectTransform.localScale = Vector3.one;
		rectTransform.anchoredPosition = Vector3.zero;
		if(!isModal)
			rectTransform.sizeDelta = Vector3.zero;
		if(isModal)
			openModalWindows++;
	}

	public virtual void Close()
	{
		GameObject.Destroy (gameObject);
		if(isModal)
			openModalWindows--;
	}
}

public class TuneLevelWindow : BaseWindow 
{
	public event System.Action<int> PathSelected;
	public event System.Action Closed;

	public List<Button> selectPathButtons;

	protected override void Start()
	{
		isModal = true;
		base.Start ();
		ButtonClicked (0);
	}

	public void ButtonClicked(int i)
	{
		selectPathButtons.ForEach(b => b.GetComponent<Image> ().color = Color.white);
		selectPathButtons[i].GetComponent<Image> ().color = Color.red;
		if (PathSelected != null)
			PathSelected (i);
	}

	List<WavePlate> GetWavePlates()
	{
		return new List<WavePlate>( GetComponentsInChildren<WavePlate> ());
	}

	public List<WaveData> GetWaveDatas()
	{
		List<WaveData> waveDatas = new List<WaveData> ();
		List<WavePlate> wavePlates = GetWavePlates ();
		foreach (var wavePlate in wavePlates) 
		{
			WaveData waveData = new WaveData ();
			int amount;
			if (int.TryParse (wavePlate.amountInput.text, out amount))
				waveData.amount = amount;
			float sum = 0;
			for (int i = 0; i < waveData.parts.Count; i++) 
			{
				float percent;
				if (i<wavePlate.percentInputs.Count && float.TryParse (wavePlate.percentInputs [i].text, out percent))
					waveData.parts [i] = percent;
				sum += waveData.parts [i];
			}
			if (sum != 100)
				for (int i = 0; i < waveData.parts.Count; i++) 
					waveData.parts[i] *= 100f / sum;
			waveDatas.Add (waveData);
		}
		return waveDatas;
	}

	public override void Close()
	{
		if (Closed != null)
			Closed ();
		base.Close ();
	}
}
