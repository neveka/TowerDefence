﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public interface ITuneLevelController
{
	event System.Action TuneLevelFinished;
}

public class TuneLevelFinishedSignal: Signal<TuneLevelFinishedSignal>
{
}

public class TuneLevelController: ITuneLevelController
{
	public event System.Action TuneLevelFinished;

	private TuneLevelWindow _window;

	private int _selectedPath;

	private readonly IPath _path;
	private readonly ITanksManager _tanksManager;
	public TuneLevelController (IPath path, ITanksManager tanksManager)
	{
		_path = path;
		_tanksManager = tanksManager;
		ShowWindow ();
	}

	void ShowWindow()
	{
		_window = GameObject.Instantiate<TuneLevelWindow> (Resources.Load<TuneLevelWindow> ("TuneLevelWindow"));
		_window.PathSelected += OnPathSelected;
		_window.Closed += OnWindowClosed;
	}

	void OnPathSelected(int i)
	{
		_selectedPath = i;
	}

	void OnWindowClosed()
	{
		_window.Closed -= OnWindowClosed;
		_window.PathSelected -= OnPathSelected;
		_path.CreateVisual ("Path" + (_selectedPath + 1));

		var waveDatas = _window.GetWaveDatas ();
		if(waveDatas.Count>0)//todo add all waves
			_tanksManager.AddWave (waveDatas[0]);

		if(TuneLevelFinished != null)
			TuneLevelFinished();
	}
}
