﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : BaseWindow 
{
	public Text moneyText;
	public Text healthText;
	public void SetMoney(int money)
	{
		moneyText.text = money.ToString ();
	}
	public void SetHealth(int health)
	{
		healthText.text = health.ToString ();
	}
}
