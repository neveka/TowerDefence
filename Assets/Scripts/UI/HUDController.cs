﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDController 
{
	private HUD _window;
	private readonly Player _player;
	public HUDController (Player player) 
	{
		_player = player;
		_player.MoneyChanged += OnMoneyChanged;
		_player.HealthChanged += OnHealthChanged;
		ShowWindow ();
	}

	public void ShowWindow()
	{
		_window = GameObject.Instantiate<HUD> (Resources.Load<HUD> ("HUD"));
		_window.SetMoney (_player.Money);
		_window.SetHealth (_player.Health);
	}

	void OnMoneyChanged(int money)
	{
		_window.SetMoney (money);
	}

	void OnHealthChanged(int health)
	{
		_window.SetHealth (health);
	}
}
